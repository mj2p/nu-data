from bitcoinrpc.authproxy import AuthServiceProxy
import pgdb
from datetime import datetime
import time
import sys

rpc_user = ''
rpc_password = ''
sleep = 30

db_host = ''
db_user = ''
db_pass = ''
db_base = ''

access = AuthServiceProxy("http://" + rpc_user + ":" + rpc_password + "@127.0.0.1:14002")

conn = pgdb.connect(host=db_host, database=db_base, user=db_user, password=db_pass)
c = conn.cursor()

#c.execute("create table if not exists data (id serial primary key, timestamp text, command text, value text)")

while True:
	c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getliquidityinfo', str(access.getliquidityinfo('B'))))
	print(str(datetime.utcnow()) + " - got liquidityinfo")
	sys.stdout.flush()
	conn.commit()
	time.sleep(sleep)


