from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import pgdb
from datetime import datetime
import os

#save the pid to an environment variable
os.environ["NU_BLOCK_PID"] = str(os.getpid())

rpc_user = ''
rpc_password = ''

db_host = ''
db_user = ''
db_pass = ''
db_base = ''

access = AuthServiceProxy("http://" + rpc_user + ":" + rpc_password + "@127.0.0.1:14002")

conn = pgdb.connect(host=db_host, database=db_base, user=db_user, password=db_pass)
c = conn.cursor()

#c.execute("create table if not exists data (id serial primary key, timestamp text, command text, value text)")
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getinfo', str(access.getinfo())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getcustodianvotes', str(access.getcustodianvotes())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getdifficulty', str(access.getdifficulty())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getmotions', str(access.getmotions())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getparkrates', str(access.getparkrates())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getpeerinfo', str(access.getpeerinfo())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getrawmempool', str(access.getrawmempool())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getvote', str(access.getvote())))
c.execute("insert into data (timestamp, command, value) values (%s, %s, %s)", (str(datetime.utcnow()), 'getparkvotes', str(access.getparkvotes())))
conn.commit()
conn.close()


