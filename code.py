import web
import pgdb

urls = (
    '/.*', 'index'
)

db_host = ''
db_user = ''
db_pass = ''
db_base = ''

def reformat_text(text):
    return text.replace("u'", "\"").replace("':", "\":").replace("',", "\",").replace("Decimal('", "").replace("')", "").replace("0E-8", "0").replace("'}", "\"}").replace("False", "false").replace("True", "true")


class index:
    def GET(self):
        #set the headers
        web.header('Content-Type', 'application/json')

        #get the input from the user
        input = web.input(cmd=None, frm=None, to=None)

        #if no parameters are passed, show this error
        if input.cmd is None:
            return '{"status": "error", "reason": "You must specify a command as the \'cmd=\' GET parameter. Acceptable values are: getinfo, getcustodianvotes, getdifficulty, getmotions, getparkrates, getpeerinfo, getrawmempool, getvote, getliquidityinfo"}'

        #if invalid cmd is passed. notify
        valid_cmds = ['getinfo', 'getcustodianvotes', 'getdifficulty', 'getmotions', 'getparkrates', 'getpeerinfo', 'getrawmempool', 'getvote', 'getliquidityinfo']
        if input.cmd not in valid_cmds:
            return '{"status": "error", "reason": "Your \'cmd\' GET parameter wasn\'t recognised"}'

        #set the upper and lower time limits
        frm = input.frm if input.frm is not None else '1970-01-01 00:00:00'
        to = input.to if input.to is not None else '9999-12-31 23:59:59'

        #connect to the database
        conn = pgdb.connect(host=db_host, database=db_base, user=db_user, password=db_pass)
        c = conn.cursor()

        #check if the count of data to check that some exists
        c.execute('select count(*) from data where command = %s and timestamp > %s and timestamp < %s', (input.cmd, frm, to))
        num_rows = c.fetchone()
        if num_rows[0] == 0:
            return '{"status": "error", "reason": "No data found"}'

        #we have some data. let's get it
        #first setup the output variable
        output = '{"status": "good", "number_of_records": ' + str(num_rows[0]) + ', "data" : ['
        c.execute('select timestamp, value from data where command = %s and timestamp > %s and timestamp < %s order by timestamp asc', (input.cmd, frm, to))
        data = c.fetchone()
        while data is not None:
            output += '{"timestamp": "' + str(data[0]) + '", "value": ' + reformat_text(data[1]) + '},'
            data = c.fetchone()
        output = output[:-1] + ']}'
        return output

application = web.application(urls, globals()).wsgifunc()
